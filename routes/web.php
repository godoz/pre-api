<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'usuario'], function() use ($router) {

    $router->post('/login', 'UsuarioController@autenticar');

    $router->group(['middleware' => 'auth'], function() use ($router) {

        $router->get('/{id}', 'UsuarioController@buscar');

        $router->get('/', 'UsuarioController@buscarTodos');

        $router->post('/', 'UsuarioController@cadastrar');

        $router->put('/', 'UsuarioController@alterar');

        $router->delete('/{id}', 'UsuarioController@remover');

    });

});

$router->group(['prefix' => 'area_atuacao', 'middleware' => 'auth'], function() use ($router) {

    $router->get('/{id}', 'AreaAtuacaoController@buscar');

    $router->get('/', 'AreaAtuacaoController@buscarTodos');

    $router->post('/', 'AreaAtuacaoController@cadastrar');

    $router->put('/{id}', 'AreaAtuacaoController@alterar');

    $router->delete('/{id}', 'AreaAtuacaoController@remover');

});
