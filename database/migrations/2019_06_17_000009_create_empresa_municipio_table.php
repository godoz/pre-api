<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaMunicipioTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'empresa_municipio';

    /**
     * Run the migrations.
     * @table empresa_municipio
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('empresa_id');
            $table->unsignedInteger('municipio_id');

            $table->index(["municipio_id"], 'fk_empresa_municipio_municipio1_idx');

            $table->primary(array('empresa_id', 'municipio_id'));

            $table->foreign('municipio_id', 'fk_empresa_municipio_municipio1_idx')
                ->references('id')->on('municipio')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
