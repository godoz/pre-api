<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'empresa';

    /**
     * Run the migrations.
     * @table empresa
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('area_atuacao_id');
            $table->unsignedInteger('usuario_id');
            $table->string('razao', 45)->nullable();
            $table->string('cnpj', 45)->nullable();
            $table->tinyInteger('porte')->nullable();
            $table->string('nome_resp', 45)->nullable();
            $table->string('email_resp', 45)->nullable();
            $table->string('fone_resp', 45)->nullable();

            $table->index(["usuario_id"], 'fk_empresa_usuario1_idx');

            $table->index(["area_atuacao_id"], 'fk_empresa_area_atuacao1_idx');


            $table->foreign('area_atuacao_id', 'fk_empresa_area_atuacao1_idx')
                ->references('id')->on('area_atuacao')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('usuario_id', 'fk_empresa_usuario1_idx')
                ->references('id')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
