<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'aluno';

    /**
     * Run the migrations.
     * @table aluno
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('curso_id');
            $table->string('nome', 45);
            $table->integer('turma');
            $table->string('pergunta', 45)->nullable();
            $table->string('resposta', 45)->nullable();

            $table->index(["curso_id"], 'fk_aluno_curso_idx');

            $table->index(["usuario_id"], 'fk_aluno_usuario1_idx');


            $table->foreign('curso_id', 'fk_aluno_curso_idx')
                ->references('id')->on('curso')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('usuario_id', 'fk_aluno_usuario1_idx')
                ->references('id')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
