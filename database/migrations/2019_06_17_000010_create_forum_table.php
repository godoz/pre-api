<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'forum';

    /**
     * Run the migrations.
     * @table forum
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('comunidade_id');
            $table->string('nome', 45);
            $table->string('descricao', 200);
            $table->tinyInteger('fixo')->default('0')->comment('0 (Não) ou 1 (Sim)');

            $table->index(["comunidade_id"], 'fk_forum_comunidade1_idx');

            $table->index(["usuario_id"], 'fk_forum_usuario1_idx');


            $table->foreign('usuario_id', 'fk_forum_usuario1_idx')
                ->references('id')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('comunidade_id', 'fk_forum_comunidade1_idx')
                ->references('id')->on('comunidade')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
