<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumMensagemTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'forum_mensagem';

    /**
     * Run the migrations.
     * @table forum_mensagem
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('forum_id');
            $table->unsignedInteger('usuario_id');
            $table->string('mensagem', 254);

            $table->index(["usuario_id"], 'fk_forum_mensagem_usuario1_idx');

            $table->index(["forum_id"], 'fk_forum_mensagem_forum1_idx');


            $table->foreign('forum_id', 'fk_forum_mensagem_forum1_idx')
                ->references('id')->on('forum')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('usuario_id', 'fk_forum_mensagem_usuario1_idx')
                ->references('id')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
