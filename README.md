# API do PRE #

## Pré requisitos ##
É necessário ter os seguintes programas instalados no servidor:
* Apache
* PHP
* Composer
## Instalação ##
Clone o sistema dentro da pasta htdocs mapeada pelo apache  
```sh
git clone https://gitlab.com/godoz/pre-api.git
```
Acesse a pasta da API e execute o comando de instalação
```sh
composer install
```
Após rodar o comando anterior faça uma cópia do arquivo **.env.example** com o nome **.env** e configure o banco de dados neste mesmo arquivo  
Execute o seguinte comando para gerar o banco de dados
```sh
php artisan migrate
```
## Testes ##
Para fazer os testes utilize a ferramenta Postman  
Importe o arquivo **PRE API.postman_collection.json** na plataforma e execute a rota Login para carregar o Token
