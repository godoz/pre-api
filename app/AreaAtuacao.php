<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaAtuacao extends Model {

    public $table = 'area_atuacao';

    public $timestamps = false;

    protected $fillable = [
        'nome'
    ];

    protected $hidden = [
    ];

    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

}
