<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Usuario extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public $table = 'usuario';

    public $timestamps = false;

    protected $fillable = [
        'email', 'tipo', 'senha', 'api_token'
    ];

    protected $hidden = [
        'senha'
    ];

    public function aluno()
    {
        return $this->hasOne('App\Aluno');
    }

    public function empresa()
    {
        return $this->hasOne('App\Empresa');
    }
}
