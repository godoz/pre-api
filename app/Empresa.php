<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model {

    public $table = 'empresa';

    public $timestamps = false;

    protected $fillable = [
        'razao', 'area_atuacao_id', 'cnpj', 'porte', 'nome_resp', 'email_resp', 'fone_resp'
    ];

    protected $hidden = [
    ];

    public function aluno()
    {
        return $this->belongsTo('App\Usuario');
    }

    public function municipios()
    {
        return $this->belongsToMany('App\Municipio');
    }

    public function areasAtuacao()
    {
        return $this->hasMany('App\AreaAtuacao');
    }

}
