<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model {

    public $table = 'municipio';

    public $timestamps = false;

    protected $fillable = [
        'nome'
    ];

    protected $hidden = [
    ];

    public function empresas()
    {
        return $this->belongsToMany('App\Empresas');
    }

}
