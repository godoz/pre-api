<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Guard;

use App\AreaAtuacao;

class AreaAtuacaoController extends Controller
{

    // Usuário logado
    private $user;

    public function __construct(Guard $auth) {

        $this->user = $auth->user();

        if ($this->user->tipo <> 1) {

            return response()->json([
                'data' => null,
                'errors' => [
                    ['message' => 'Usuário sem permissão']
                ]
            ]);

        }
    }

    // Busca todos os usuários
    public function buscarTodos() {

        $areas_atuacao = AreaAtuacao::all();

        return response()->json([
            'data' => $areas_atuacao
        ]);

    }

    // Busca apenas um usuário
    public function buscar($id) {

        $area_atuacao = AreaAtuacao::find($id);

        return response()->json([
            'data' => $area_atuacao
        ]);

    }

    // Remove um usuário
    public function remover($id) {

        $area_atuacao = AreaAtuacao::find($id);

        $area_atuacao->delete();

        return response()->json([
            'data' => true
        ]);

    }

    // Cadastra um usuário
    public function cadastrar(Request $request) {

        $dados_area_atuacao = $this->validate($request, [
            'nome' => 'required|string|max:45'
        ]);

        $area_atuacao = new AreaAtuacao($dados_area_atuacao);

        $area_atuacao->save();

        return $area_atuacao;

    }

    // Altera um usuário
    public function alterar($id, Request $request) {

        $dados_area_atuacao = $this->validate($request, [
            'nome' => 'required|string|max:45'
        ]);

        $area_atuacao = AreaAtuacao::find($id);

        $area_atuacao->update($dados_area_atuacao);

        return $area_atuacao;

    }

}
