<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Guard;

use App\Usuario;

class UsuarioController extends Controller
{

    // Usuário logado
    private $user;

    public function __construct(Guard $auth) {
        $this->user = $auth->user();
    }

    // Autentica o usuário
    public function autenticar(Request $request) {

        $usuario = Usuario::where('email', $request->input('email'))->first();

        if ($usuario && Hash::check($request->input('senha'), $usuario->senha)) {

            $token = str_random(254);

            $usuario->update(['api_token' => $token]);

            return response()->json([
                'data' => $usuario
            ]);

        } else {

            return response()->json([
                'data' => null,
                'errors' => [
                    ['message' => 'Autenticação inválida']
                ]
            ]);

        }

    }

    // Busca todos os usuários
    public function buscarTodos() {

        if ($this->user->tipo <> 1) {
            return response()->json([
                'data' => null,
                'errors' => [
                    ['message' => 'Usuário sem permissão']
                ]
            ]);
        }

        return response()->json([
            'data' => Usuario::all()
        ]);

    }

    // Busca apenas um usuário
    public function buscar($id) {

        if ($this->user->tipo <> 1) {

            return response()->json([
                'data' => null,
                'errors' => [
                    ['message' => 'Usuário sem permissão']
                ]
            ]);

        }

        $usuario = Usuario::find($id);

        return response()->json([
            'data' => $usuario
        ]);

    }

    // Remove um usuário
    public function remover($id) {

        if ($this->user->tipo <> 1) {

            return response()->json([
                'data' => null,
                'errors' => [
                    ['message' => 'Usuário sem permissão']
                ]
            ]);

        }

        $usuario = Usuario::find($id);

        $usuario->delete();

        return response()->json([
            'data' => true
        ]);

    }

    // Cadastra um usuário
    public function cadastrar(Request $request) {

        $dados_usuario = $this->validate($request, [
            'email' => 'required|email|unique:usuario',
            'senha' => 'required|string|min:3|max:25',
            'tipo' => 'required|integer|between:1,3'
        ]);

        $usuario = new Usuario($dados_usuario);

        $usuario->save();

        if ($usuario->tipo == 2) {

            $dados_empresa = $this->validate($request, [
                'area_atuacao_id' => 'required|exists:area_atuacao,id',
                'razao' => 'required|string|max:45',
                'cnpj' => 'required|digits_between:1,14',
                'porte' => 'required|integer|between:1,4',
                'nome_resp' => 'required|string|max:45',
                'email_resp' => 'required|email',
                'fone_resp' => 'required',
                'municipios' => 'required|array',
                'municipios.*' => 'exists:municipio,id'
            ]);

            $usuario->empresa()->create($dados_empresa);

            foreach ($dados_empresa['municipios'] as $municipio) {
                $usuario->empresa->municipios()->attach($municipio);
            }

            $usuario->empresa->municipios;

        }

        if ($usuario->tipo == 3) {

            $dados_aluno = $this->validate($request, [
                'nome' => 'required|string|max:45',
                'turma' => 'required|integer',
                'curso_id' => 'required|exists:curso,id',
                'pergunta' => 'required|string|max:45',
                'resposta' => 'required|string|max:45'
            ]);

            $usuario->aluno()->create($dados_aluno);

            $usuario->aluno;

        }

        return $usuario;

    }

}
