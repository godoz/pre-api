<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model {

    public $table = 'aluno';

    public $timestamps = false;

    protected $fillable = [
        'nome',  'curso_id', 'turma', 'pergunta', 'resposta'
    ];

    protected $hidden = [
        'pergunta', 'resposta'
    ];

    public function aluno()
    {
        return $this->belongsTo('App\Usuario');
    }

}
